import math
import numpy as np
import matplotlib.pyplot as plt

moon_distance = 384e6  # m

def forward_euler():
    h = 0.1 # s
    g = 9.81 # m / s2

    num_steps = 50

    t = np.zeros(num_steps + 1)
    x = np.zeros(num_steps + 1)
    v = np.zeros(num_steps + 1)

    for step in range(num_steps):
        t[step + 1] = h*(step + 1)## your code here
        x[step + 1] = x[step] + h * v[step]## your code here
        v[step + 1] = v[step] - h * g ## your code here
    return t, x, v

t, x, v = forward_euler()

axes_height = plt.subplot(211)
plt.plot(t, x)
axes_velocity = plt.subplot(212)
plt.plot(t, v)
axes_height.set_ylabel("Height in m")
axes_velocity.set_ylabel("Velocity in m/s2")
axes_velocity.set_xlabel("Time in s")
plt.show()

