import math
import numpy
import matplotlib.pyplot


moon_distance = 384e6 # m

def orbit():
    num_steps = 29
    x = numpy.zeros([num_steps + 1, 2])
    # INSERT CODE HERE
    # Use math.sin(angle) and math.cos(angle) with angle in radians
    for step in range(num_steps):
        cos = math.cos((2 * math.pi)/28*step) * moon_distance
        sin = math.sin((2 * math.pi)/28*step) * moon_distance
        x[step+1][0] = cos
        x[step+1][1] = sin
    # for step in numpy.linspace(0,28,100):
    #     t[step+1] = t[step] + step
    #     x[step+1][0] = x[step]+
    x[0][0] = moon_distance
    return x

x = orbit()
def plot_me():
    matplotlib.pyplot.axis('equal')
    matplotlib.pyplot.plot(x[:, 0], x[:, 1])
    axes = matplotlib.pyplot.gca()
    axes.set_xlabel('Longitudinal position in m')
    axes.set_ylabel('Lateral position in m')
    matplotlib.pyplot.show()

plot_me()