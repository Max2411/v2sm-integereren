import numpy as np
import matplotlib.pyplot as plt

h = 0.1 # s
earth_mass = 5.97e24 # kg
gravitational_constant = 6.67e-11 # N m2 / kg2


def acceleration(spaceship_position):
    vector_to_earth = - spaceship_position # earth at origin
    return gravitational_constant * earth_mass / np.linalg.norm(vector_to_earth) ** 3 * vector_to_earth


def ship_trajectory():
    num_steps = 130000 # Feel free to experiment with a lower number of steps
    x = np.zeros([num_steps + 1, 2]) # m
    v = np.zeros([num_steps + 1, 2]) # m/s

    x[0, 0] = 15e6 # start values of spacecraft
    x[0, 1] = 1e6
    v[0, 0] = 2e3
    v[0, 1] = 4e3
    ### TODO: Insert Forward Euler Method here
    for step in range(num_steps):
        x[step + 1] = x[step] + h * v[step]
        v[step + 1] = v[step] + h * acceleration(x[step])
    return x, v


x, v = ship_trajectory()


def plot_me():
    plt.plot(x[:, 0], x[:, 1])
    plt.scatter(0, 0)
    plt.axis('equal')
    axes = plt.gca()
    axes.set_xlabel('Longitudinal position in m')
    axes.set_ylabel('Lateral position in m')
    plt.show()


plot_me()